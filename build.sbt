name := "hpcl-cards-scrape"

version := "0.1"

scalaVersion := "2.12.7"

libraryDependencies ++= Seq(
  "net.ruippeixotog" %% "scala-scraper" % "2.1.0",
  "mysql" % "mysql-connector-java" % "8.0.13",
  "org.scalikejdbc" %% "scalikejdbc" % "2.5.2",
  "ch.qos.logback" % "logback-classic" % "1.2.3"
)
