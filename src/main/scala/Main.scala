
object Main {
  def main(args: Array[String]): Unit = {
    val fileLocation = args.headOption.getOrElse("/Users/saswatdutta/Documents/fuel/hpcl/DriveTrackPlus_card_limits.html")
    val cards = extractUnmappedCards(fileLocation)
    upload(cards)
  }

  def extractUnmappedCards(fileLocation: String): Seq[String] = {
    import net.ruippeixotog.scalascraper.browser.JsoupBrowser
    import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
    import net.ruippeixotog.scalascraper.dsl.DSL._
    import net.ruippeixotog.scalascraper.model.Element

    def isUnmappedCard(tr: Element): Boolean = {
      val phone = extractNthTdText(tr, 3)
      val vehicle = extractNthTdText(tr, 2)
      phone.isEmpty && vehicle.startsWith("VYOM")
    }

    def extractNthTdText(tr: Element, pos: Int): String = {
      val tds = tr >> elementList("td")
      tds.lift(pos).map(_.text.trim).getOrElse("")
    }

    val browser = JsoupBrowser()
    val doc = browser.parseFile(fileLocation)

    val tableRowsSelector = "#ctl00_phM_gvSearchCard > tbody > tr:not(:first-child)"
    val trs = doc >> elementList(tableRowsSelector)

    // card nos
    trs.filter(isUnmappedCard).map(it => extractNthTdText(it, 1))
  }

  def upload(cards: Seq[String]): Unit = {
    import scalikejdbc._
    //    Class.forName("com.mysql.cj.jdbc.Driver") // usually auto loaded
    ConnectionPool.singleton(
      "jdbc:mysql://vyom-payments.cqgxnxnhglky.ap-south-1.rds.amazonaws.com:3306/vyom_payments?useSSL=false",
      "vyom_payments_api",
      "prod@vyompayments")
    implicit val session = AutoSession

    var count = 0
    cards foreach { card =>
      try {
        sql"""
         INSERT INTO `vyom_payments`.`hpcl_ccms_card_mapping`
         (`created_at`, `updated_at`, `card_number`, `is_active`)
         VALUES (unix_timestamp()*1000, unix_timestamp()*1000, ${card}, b'1')
         """.update.apply()
        count += 1
        println(s"${count}> ${card}")
      } catch {
        case ex => println(s"! Error in insert of ${card} : ", ex.getMessage)
      }
    }
    println(s"Uploaded ${count} cards")
  }
}
